'use strict';

angular.module('myApp.view_home', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view_home', {
    templateUrl: 'view_home/view_home.html',
    controller: 'View_homeCtrl'
  });
}])

.controller('View_homeCtrl', [function() {

}])
