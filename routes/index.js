var express = require('express');
var router = express.Router();
var path = require('path');

var handlebars = require('handlebars');
 
/* GET home page. */
router.get('/', function(req, res, next) {
  res.sendFile(path.join(__dirname, '../', 'views', 'index.html'));
});
 
router.get('/data', function(req,res){

     // get your data into a variable
    var resultDataJson = {
                    "foo": [
                        {
                            "bar": "Hello World Hasndle bar!"
                        },
                        {
                            "bar": " Theis Single Line came from handle bar"
                        }
                    ]
                  };        
    // set up your handlebars template
    var stringTemplate = '{{#each foo}}{{this.bar}} yeahhh {{/each}}';
    // compile the template
    var template = handlebars.compile(stringTemplate);
    // call template as a function, passing in your data as the context
    var outputString = template(resultDataJson);
    

    
    
    
    res.json([{"id": 1, "name": outputString, "city": "Pantano do Sul"},
        {"id": 2, "name": "Skyble", "city": "Guilmaro"},
        {"id": 3, "name": "Tagfeed", "city": "Gnosjö"},
        {"id": 4, "name": "Realcube", "city": "Jrashen"},
        {"id": 5, "name": "Bluejam", "city": "Zhangjiawo"},
        {"id": 6, "name": "Jayo", "city": "Obonoma"},
        {"id": 7, "name": "Cogidoo", "city": "Sungsang"},
        {"id": 8, "name": "Avavee", "city": "Diawara"},
        {"id": 9, "name": "Tagtune", "city": "Monywa"},
        {"id": 10, "name": "Centimia", "city": "Retkovci"}]);
    
    
    
    
    
});

module.exports = router;